class Code
  attr_reader :pegs

  PEGS = { "R" => :red, "G" => :green, "B" => :blue, "Y" => :yellow,
            "O" => :orange, "P" => :purple }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    pegs = str.chars.map do |ch|
      raise "parse error" unless PEGS.keys.include?(ch.upcase)
      PEGS[ch.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    Code.new(pegs)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(user)
    exact_matches = 0
    pegs.each_index do |i|
      exact_matches += 1 if self[i] == user[i]
    end

    exact_matches
  end

  def near_matches(user)
    user_counts = user.color_counts
    near_matches = 0
    self.color_counts do |color, count|
      next unless user_counts.keys.include?(color)
      near_matches += [count, user_counts[color]].min
    end
    near_matches

  end

  def ==(user)
    return false unless user.is_a?(Code)
    self.pegs == user.pegs
  end

  protected

  def color_counts
    color_counts = Hash.new(0)
    @pegs.each do |color|
      color_counts[color] += 1
    end
  end


end

class Game
  attr_reader :secret_code
end
